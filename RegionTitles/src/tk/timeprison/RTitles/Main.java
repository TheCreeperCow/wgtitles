package tk.timeprison.RTitles;

import java.io.File;
import java.util.List;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import net.md_5.bungee.api.ChatColor;

public class Main extends JavaPlugin{
	public List<String> regions;
	public static FileConfiguration config;
	@Override
	public void onEnable() {
		Logger logger = Logger.getLogger("Minecraft");
		logger.info("Worldguard Titles activated");
		Bukkit.getPluginManager().registerEvents(new Events(), this);
		createConfig();
	}
	
	@Override
	public void onDisable() {
		Logger logger = Logger.getLogger("Minecraft");
		logger.info("Worldguard titles Disabled");
	}
	private void createConfig() {
        try {
            if (!getDataFolder().exists()) {
                getDataFolder().mkdirs();
            }
            File file = new File(getDataFolder(), "config.yml");
            if (!file.exists()) {
                getLogger().info("Config.yml not found, creating!");
                saveDefaultConfig();
                reloadConfig();
                config = getConfig();
            } else {
                getLogger().info("Config.yml found, loading!");
                config = getConfig();
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

    }

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(label.equalsIgnoreCase("wgtitle")) {
			if(args.length == 0) {
				return false;
			}
			else {
				if(args[0].equals("reload")){
					reloadConfig();
					saveConfig();
					config = getConfig();
					sender.sendMessage(ChatColor.GREEN+"Config reloaded");
					return true;
				}
			}
		}
		return false;
	}
	
}
