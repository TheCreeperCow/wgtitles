package tk.timeprison.RTitles;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.mewin.WGRegionEvents.events.RegionEnterEvent;
import com.mewin.WGRegionEvents.events.RegionLeaveEvent;
import com.sk89q.worldguard.protection.flags.Flag;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import net.md_5.bungee.api.ChatColor;

public class Events implements Listener{
	@EventHandler
	public void onRegionEnterEvent(RegionEnterEvent e) {
		if(Main.config.getString(e.getRegion().getId()+".EnterMain")==null&&Main.config.getString(e.getRegion().getId()+".EnterSub")==null) {
			if(e.getRegion().getId().equalsIgnoreCase("__global__")) {
				return;
			}
			String main = Main.config.getString("RegionEnterMain");
			main = main.replaceAll("\\{player\\}", e.getPlayer().getName());
			main = main.replaceAll("\\{region\\}", e.getRegion().getId());
			String sub = Main.config.getString("RegionEnterSub");
			sub = sub.replaceAll("\\{player\\}", e.getPlayer().getName());
			sub = sub.replaceAll("\\{region\\}", e.getRegion().getId());
			e.getPlayer().sendTitle(ChatColor.translateAlternateColorCodes('&', main), ChatColor.translateAlternateColorCodes('&', sub));
		} else{
			String main = Main.config.getString(e.getRegion().getId()+".EnterMain");
			main = main.replaceAll("\\{player\\}", e.getPlayer().getName());
			main = main.replaceAll("\\{region\\}", e.getRegion().getId());
			String sub = Main.config.getString(e.getRegion().getId()+".EnterSub");
			sub = sub.replaceAll("\\{player\\}", e.getPlayer().getName());
			sub = sub.replaceAll("\\{region\\}", e.getRegion().getId());
			e.getPlayer().sendTitle(ChatColor.translateAlternateColorCodes('&', main), ChatColor.translateAlternateColorCodes('&', sub));
		}
	}
	@EventHandler
	public void OnRegionLeaveEvent(RegionLeaveEvent e) {
		if(Main.config.getString(e.getRegion().getId()+".LeaveMain")==null&&Main.config.getString(e.getRegion().getId()+".LeaveSub")==null) {
			String main = Main.config.getString("RegionLeaveMain");
			main = main.replaceAll("\\{player\\}", e.getPlayer().getName());
			main = main.replaceAll("\\{region\\}", e.getRegion().getId());
			String sub = Main.config.getString("RegionLeaveSub");
			sub = sub.replaceAll("\\{player\\}", e.getPlayer().getName());
			sub = sub.replaceAll("\\{region\\}", e.getRegion().getId());
			e.getPlayer().sendTitle(ChatColor.translateAlternateColorCodes('&', main), ChatColor.translateAlternateColorCodes('&', sub));
		} else{
			String main = Main.config.getString(e.getRegion().getId()+".LeaveMain");
			main = main.replaceAll("\\{player\\}", e.getPlayer().getName());
			main = main.replaceAll("\\{region\\}", e.getRegion().getId());
			String sub = Main.config.getString(e.getRegion().getId()+".LeaveSub");
			sub = sub.replaceAll("\\{player\\}", e.getPlayer().getName());
			sub = sub.replaceAll("\\{region\\}", e.getRegion().getId());
			e.getPlayer().sendTitle(ChatColor.translateAlternateColorCodes('&', main), ChatColor.translateAlternateColorCodes('&', sub));
		}
	}
}
